<?php
namespace Avris\TimeDiff;

class TimeDiffModuleTest extends \PHPUnit_Framework_TestCase
{
    public function testModule()
    {
        $module = new TimeDiffModule();
        $config = $module->extendConfig('test', __DIR__);
        $this->assertSame(
            ['timeDiff', 'timeDiffTwig', 'timeDiffLocaleSet'],
            array_keys($config['services'])
        );
    }
}
