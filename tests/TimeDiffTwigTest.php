<?php
namespace Avris\TimeDiff;

class TimeDiffTwigTest extends \PHPUnit_Framework_TestCase
{
    public function testFilter()
    {
        $diff = new TimeDiff();
        $twig = new TimeDiffTwig($diff);

        /** @var \Twig_SimpleFilter $filter */
        $filter = $twig->getFilters()[0];

        $this->assertSame('timeDiff', $filter->getName());
        $callable = $filter->getCallable();

        $this->assertSame(
            '5 minutes ago',
            (string) $callable('2016-12-17 7:55', '2016-12-17 8:00')
        );
    }
}
