<?php
use Avris\Bag\Bag;
use Avris\Micrus\Localizator\Locale\Locale;
use Avris\Micrus\Localizator\Locale\YamlLocaleSet;
use Avris\Micrus\Localizator\Localizator;
use Avris\Micrus\Localizator\Order\TranslationOrder;
use Avris\Micrus\Localizator\Selector\CountVersion;
use Avris\Micrus\Localizator\String\LocalizedString;

foreach (['', '/..', '/../..', '/../../..', '/../../../..'] as $dir) {
    if (file_exists($autoloader = __DIR__ . $dir . '/vendor/autoload.php')) {
        require_once $autoloader;
        break;
    }
}

foreach (glob(__DIR__ . '/_help/*.php') as $file) {
    require_once $file;
}

$localizator = new Localizator(
    [new YamlLocaleSet('timeDiff', __DIR__ . '/../src/Translations', 'en')],
    new TranslationOrder(new Bag(['en' => 'English']), [new Locale('en')]),
    new Bag(['en' => 'English']),
    [new CountVersion()],
    []
);

LocalizedString::setLocalizator($localizator);
