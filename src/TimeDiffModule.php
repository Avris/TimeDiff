<?php
namespace Avris\TimeDiff;

use Avris\Micrus\Bootstrap\Module;
use Avris\Micrus\Localizator\Locale\YamlLocaleSet;

class TimeDiffModule implements Module
{
    public function extendConfig($env, $rootDir)
    {
        return [
            'services' => [
                'timeDiff' => [
                    'class' => TimeDiff::class,
                ],
                'timeDiffTwig' => [
                    'class' => TimeDiffTwig::class,
                    'params' => ['@timeDiff'],
                    'tags' => ['twigExtension'],
                ],
                'timeDiffLocaleSet' => [
                    'class' => YamlLocaleSet::class,
                    'params' => ['timeDiff', __DIR__ . '/Translations', 'en'],
                    'tags' => ['localeSet'],
                ],
            ],
        ];
    }
}
