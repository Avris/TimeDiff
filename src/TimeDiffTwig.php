<?php
namespace Avris\TimeDiff;

class TimeDiffTwig extends \Twig_Extension
{
    /** @var TimeDiff */
    protected $timeDiff;

    public function __construct(TimeDiff $timeDiff)
    {
        $this->timeDiff = $timeDiff;
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('timeDiff', function ($datetime, $now = null) {
                return $this->timeDiff->diff($datetime, $now);
            })
        ];
    }
}
