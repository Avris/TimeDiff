testBase = '2016-12-17 8:00'
testCases = {
  '2013-03-05 5:00': '4 years ago',
  '2016-03-05 5:00': '9 months ago',
  '2016-12-05 5:00': '12 days ago',
  '2016-12-13 19:00': '4 days ago',
  '2016-12-15 15:00': '2 days ago',
  '2016-12-16 15:00': 'yesterday',
  '2016-12-17 5:00': 'about 3 hours ago',
  '2016-12-17 7:00': 'about an hour ago',
  '2016-12-17 7:30': 'about half an hour ago',
  '2016-12-17 7:55': '5 minutes ago',
  '2016-12-17 7:59': 'a minute ago',
  '2016-12-17 7:59:45': 'now',
  '2016-12-17 8:01': 'in a minute',
  '2016-12-17 8:04': 'in 4 minutes',
  '2016-12-17 8:42': 'in about half an hour',
  '2016-12-17 9:01': 'in about an hour',
  '2016-12-17 15:12': 'in about 7 hours',
  '2016-12-18 15:00': 'tomorrow',
  '2016-12-19 15:00': 'in 2 days',
  '2016-12-24 15:12': 'in 7 days',
  '2017-05-24 15:12': 'in 5 months',
  '2023-12-24 15:12': 'in 7 years',
}

diff = new TimeDiff
for input, expectedOutput of testCases
  output = diff.diff(input, testBase)
  console.log "#{input} -> #{output} (#{if output == expectedOutput then 'OK' else 'FAIL, expected: ' + expectedOutput})"
