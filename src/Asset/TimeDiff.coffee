class @TimeDiff
  diff: (datetime, now = new Date) ->
    [isFuture, diff, diffDays] = getDiffs(datetime, now)
    switch
      when diff < 30
        trans('now')
      when diffDays == 0
        wrap(relativeOneDay(diff), isFuture)
      when diffDays == 1
        trans(if isFuture then 'tomorrow' else 'yesterday')
      when diffDays == 2
        trans(if isFuture then 'afterTomorrow' else 'beforeYesterday')
      else
        wrap(relativeManyDays(diffDays), isFuture)

  # Private
  getDiffs = (datetime, now) ->
    datetime = new Date(datetime)
    now = new Date(now)
    diff = parseInt((datetime - now) / 1000)
    diffDays = parseInt((datetime.setHours(0,0,0,0) - now.setHours(0,0,0,0)) / (1000 * 60 * 60 * 24))
    diffDays = 0 if Math.abs(diff) < 60 * 60 * 6
    return [diff > 0, Math.abs(diff), Math.abs(diffDays)]
  relativeOneDay = (diff) ->
    minutes = Math.round(diff / 60)
    switch
      when minutes < 20
        trans('minutes', {count: minutes})
      when minutes < 45
        about(trans('halfHour'))
      when minutes < 80
        about(trans('hours', {count: 1}))
      when minutes < 100
        about(trans('hourAndHalf'))
      else
        about(trans('hours', {count: Math.round(diff / (60 * 60))}))
  relativeManyDays = (diffDays) ->
    switch
      when diffDays <= 31
        trans('days', {count: diffDays})
      when diffDays <= 365
        trans('months', {count: Math.round(diffDays / 30.5)})
      else
        trans('years', {count: Math.round(diffDays / 365.25)})
  wrap = (text, isFuture) ->
    trans((if isFuture then 'wrapAfter' else 'wrapBefore'), {span: text})
  about = (text) ->
    trans('wrapAbout', {span: text})
  trans = (key, params = []) ->
    M.l('timeDiff.' + key, params)
