## Avris TimeDiff ##

    $timeDiff = new TimeDiff();
    echo $timeDiff->diff('2017-03-04 20:56');

Will echo a text like "now", "5 minutes ago", "2 hours ago" etc., depending on current time.

Translations will be handled by the [Micrus Localizator](https://gitlab.com/Avris/Micrus-Localizator).
If it's not present, please implement yourself a function `l($word, $replacements = [])` that will
translate strings from files `src/Translations/*.yml`.

### Copyright ###

* **Author:** Andrzej Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
