<?php
namespace Avris\Micrus\Bootstrap;

interface Module
{
    /**
     * @return array
     */
    public function extendConfig($env, $rootDir);
}
