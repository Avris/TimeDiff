<?php
namespace Avris\TimeDiff;

class TimeDiff
{
    /**
     * @param string|\DateTime $datetime
     * @param string|\DateTime|null $now
     * @return string
     */
    public function diff($datetime, $now = null)
    {
        list($isFuture, $diff, $diffDays) = $this->getDiffs($datetime, $now);

        if ($diff < 30) {
            return $this->trans('now');
        } elseif ($diffDays == 0) {
            return $this->wrap($this->relativeOneDay($diff), $isFuture);
        } elseif ($diffDays == 1) {
            return $this->trans($isFuture ? 'tomorrow' : 'yesterday');
        } elseif ($diffDays == 2) {
            return $this->trans($isFuture ? 'afterTomorrow' : 'beforeYesterday');
        }

        return $this->wrap($this->relativeManyDays($diffDays), $isFuture);
    }

    protected function getDiffs($dateTime, $now)
    {
        $now = $now instanceof \DateTime ? clone $now : new \DateTime($now);
        $dateTime = $dateTime instanceof \DateTime ? clone $dateTime : new \DateTime($dateTime);

        $diff = $dateTime->format('U') - $now->format('U');
        $diffDays = ($dateTime->modify('midnight')->format('U') - $now->modify('midnight')->format('U')) / (60 * 60 * 24);

        // up to six hours difference, we don't call it "yesterday"/"tomorrow"
        // even if there was midnight in this period
        if (abs($diff) < 60 * 60 * 6) {
            $diffDays = 0;
        }

        return [$diff > 0, abs($diff), abs(round($diffDays))];
    }

    protected function relativeOneDay($diff)
    {
        $minutes = round($diff / 60);

        if ($minutes < 20) {
            return $this->trans('minutes', ['count' => $minutes]);
        } elseif ($minutes < 45) {
            return $this->about($this->trans('halfHour'));
        } elseif ($minutes < 80) {
            return $this->about($this->trans('hours', ['count' => 1]));
        } elseif ($minutes < 100) {
            return $this->about($this->trans('hourAndHalf'));
        }

        return $this->about($this->trans('hours', ['count' => round($diff / (60 * 60))]));
    }

    protected function relativeManyDays($diffDays)
    {
        if ($diffDays <= 31) {
            return $this->trans('days', ['count' => $diffDays]);
        } elseif ($diffDays <= 365) {
            return $this->trans('months', ['count' => round($diffDays / 30.5)]);
        }

        return $this->trans('years', ['count' => round($diffDays / 365.25)]);
    }

    protected function wrap($text, $isFuture)
    {
        return $this->trans($isFuture ? 'wrapAfter' : 'wrapBefore', ['span' => $text]);
    }

    protected function about($text)
    {
        return $this->trans('wrapAbout', ['span' => $text]);
    }

    protected function trans($key, $params = [])
    {
        return (string) l('timeDiff.' . $key, $params);
    }
}
